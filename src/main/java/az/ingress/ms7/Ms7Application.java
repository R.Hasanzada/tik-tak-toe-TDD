package az.ingress.ms7;

import az.ingress.ms7.properties.ApplicationDetails;
import az.ingress.ms7.spring.way.Animal;
import az.ingress.ms7.spring.way.Student;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms7Application implements CommandLineRunner {

	private final Animal animal;

	private final Animal animal1;

	private final ApplicationDetails app;

	public static void main(String[] args) {
		SpringApplication.run(Ms7Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("animal is " + animal);
		System.out.println("animal is " + animal1);

		System.out.println("appNane " + app.getApplicationName() );
		System.out.println("version " + app.getVersion() );
		System.out.println("version " + app.getDeveloperNames() );

		Student student = Student.builder()
				.age(25L)
				.name("Ruslan").build();

	}
}
