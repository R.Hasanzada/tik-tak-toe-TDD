package az.ingress.ms7.creational_pattern;

public interface Animal {

    void makeNoise();
}
