package az.ingress.ms7.creational_pattern;

public class FactoryMethod {

    public static Animal getinstance(){
        return new Dog();
    }
}
