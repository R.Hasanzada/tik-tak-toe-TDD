package az.ingress.ms7.creational_pattern;

public class Main {

    public static void main(String[] args) {
        BillPughSingleton s = BillPughSingleton.getInstance();
        BillPughSingleton s1 = BillPughSingleton.getInstance();
        System.out.println(s);
        System.out.println(s1 );

        Animal a = FactoryMethod.getinstance();

    }
}
