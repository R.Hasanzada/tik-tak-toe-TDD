package az.ingress.ms7.spring.way;

public interface Animal {

    void makeNoise();
}
