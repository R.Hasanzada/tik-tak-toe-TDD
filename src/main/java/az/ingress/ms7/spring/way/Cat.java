package az.ingress.ms7.spring.way;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("cat")
@Scope("prototype")
@Primary
public class Cat implements Animal {

    @Override
    public void makeNoise() {

    }
}
