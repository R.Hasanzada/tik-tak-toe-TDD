package az.ingress.ms7.spring.way;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("dog")
public class Dog implements Animal {

    @Override
    public void makeNoise() {

    }
}
