package az.ingress.ms7.spring.way;

import lombok.Builder;

@Builder
public class Student {

    private String name;
    private String title;
    private String email;
    private Long age;
}
