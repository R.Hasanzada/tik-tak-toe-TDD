package az.ingress.ms7.tdd;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorImplTest {

    private Calculator calculator;

    @Test
    public void givenAAndBWhenAddThenC(){

        //arrange
        calculator = new CalculatorImpl();
        int a = 5;
        int b = 2;
        int c = 7;

        //act
        int result = calculator.add(a,b);

        //assert
        Assertions.assertThat(result).isEqualTo(c);
    }

    @Test
    public void givenAAndBWhenAddThenC2(){
        //arrange
        calculator = new CalculatorImpl();
        int a = 5;
        int b = 2;
        int c = 7;

        //act
        int result = calculator.add(a,b);

        //assert
        Assertions.assertThat(result).isEqualTo(c);
    }

  @Test
    public void givenAAndBWhenDivideThenC(){
        //arrange
        calculator = new CalculatorImpl();
        int a = 5;
        int b = 0;

        //act
        assertThatThrownBy(() -> calculator.divide(a,b))
                .isInstanceOf(ArithmeticException.class);
    }

}