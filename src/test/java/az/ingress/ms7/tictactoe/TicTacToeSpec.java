package az.ingress.ms7.tictactoe;

import az.ingress.ms7.tdd.TicTacToe;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TicTacToeSpec {

    private TicTacToe ticTacToe;


    @BeforeEach
    public void before() {
        ticTacToe = new TicTacToe();
    }

    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        Assertions.assertThatThrownBy(() -> ticTacToe.play(5, 2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenYOutsideBoardThenRuntimeException() {
        Assertions.assertThatThrownBy(() -> ticTacToe.play(2, 5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        ticTacToe.play(1, 2);

        Assertions.assertThatThrownBy(() -> ticTacToe.play(1, 2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        Assertions.assertThat(ticTacToe.nextPlayer())
                .isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenY() {
        ticTacToe.play(1, 1);
        Assertions.assertThat(ticTacToe.nextPlayer())
                .isEqualTo('Y');
    }

    @Test
    public void whenPlayThenNoWinner() {
        String actual = ticTacToe.play(1, 1);
        Assertions.assertThat(actual).isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // Y
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // Y
        String actual = ticTacToe.play(3, 1); // X
        Assertions.assertThat(actual).isEqualTo("X is the winner");

    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // Y
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // Y
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        Assertions.assertThat(actual).isEqualTo("Y is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // Y
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // Y
        String actual = ticTacToe.play(3, 3); // Y
        Assertions.assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        String actual = ticTacToe.play(3, 1); // O
        Assertions.assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        Assertions.assertThat(actual).isEqualTo("The result is draw");
    }


}
